University of Bath Computer Science Poster Template
=======================================================

This is a template for the poster that Bath University Computer
Science students have to give near the end of their final year
project. That said this could easily be adapted for another department
at the university or even a different university.

Examples
----------

Example documents generated from the template are in the Examples
folder. `Here's`__ the poster and `here's`__ the version for the printers
with crop lines.

.. __: https://bitbucket.org/pwr22/bath-university-poster-template/src/HEAD/Examples/poster.pdf?at=master
.. __: https://bitbucket.org/pwr22/bath-university-poster-template/src/HEAD/Examples/print.pdf?at=master

Installation
-------------

Open up a terminal and run the following ::

  git clone https://pwr22@bitbucket.org/pwr22/bath-university-poster-template.git

Then just run this to build ::

  make

Depends on the beamer class. Also requires the beamerposter, epstopdf,
calc and crop packages.

Why
----

The inspiration for this was that the LaTeX template we were provided
isn't a template at all. It is infact a full poster and it is a
horrible mess of code. The layout was also landscape despite the
advisement that we make a portrait version.

What should be provided is a theme that handles
as much as the layout for you as possible, but still allowing you
control, so that you can focus on the content. This is what I'm aiming
to do here.

License
--------

This template is extended from styles provided by the beamer class and
is thus licensed under the GPL but please see LICENSE for full details.
